const express = require('express');

const routes = express.Router();

const TweetConstroller = require('./controllers/TweetController');
const LikeConstroller = require('./controllers/LikeController');



routes.get('/tweets', TweetConstroller.index);
routes.post('/tweets', TweetConstroller.store);
routes.post('/likes/:id', LikeConstroller.store);

module.exports = routes;