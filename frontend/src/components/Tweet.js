import React, { Component } from 'react';

import './Tweet.css'
import like from '../like.svg'

import api from '../services/api';

export default class Tweet extends Component {

  handleLike = async e => {
    const { _id } =  this.props.tweetProp;
    //console.log({_id})
   

    await api.post(`likes/${_id}`);
  }


  render() {
  const { tweetProp } =  this.props;

    return (
      <li className="tweet">
        <strong>{tweetProp.author}</strong>
        <p>{tweetProp.content}</p>
        <button type='button' onClick={this.handleLike}>
          <img src={like} alt='like'/>
          {tweetProp.likes}
        </button>
      </li>
    )
  }
}
